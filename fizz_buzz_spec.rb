require "./fizz_buzz"

describe "fizz_buzz" do
  let(:user) {Fizzbuzz.new}

  describe "#run" do
    [1,2] .each do |num|
  	  context "when argument is #{num}" do
  	    it "returns #{num}" do
  	  	  expect(user.run(num)).to eq num
  	    end
  	  end
    end

    (1..33) .each do |num|
      context "when argument is mltiples of 3" do
        before {hoge = num * 3}
        it "returns fizz" do
          expect(user.run(hoge)).to eq "fizz"
        end
      end
    end

    [5,10] .each do |num|
      context "when argument is mltiples of 5" do
        it "returns buzz" do
          expect(user.run(num)).to eq "buzz"
        end
      end
    end

    context "when argument is 15" do
      it "returns fizzbuzz" do
        expect(user.run(15)).to eq "fizzbuzz"
      end
    end
  end
end

=begin
describe 'テスト対象' do
  describe 'テスト対象メソッド' do
    context '与える入力' do
      it '期待する出力'
    end
  end
end
=end
